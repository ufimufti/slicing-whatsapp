package com.example.whatsappaul

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(fa: FragmentManager): FragmentPagerAdapter(fa){

    private val pages = listOf(
            CallFragment(),
            ChatFragment(),
            ContactFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "CALLS"
            1 -> "CHATS"
            else -> "CONTACTS"
        }
    }
}